"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPLAN_SMALL = "Small";
const gPLAN_MEDIUM = "Medium";
const gPLAN_LARGE = "Large";

const gPLAN_HAI_SAN = "HaiSan";
const gPLAN_HAWAI = "Hawai";
const gPLAN_BACON = "Bacon";


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();

    $("#btn-small").on("click", function () {
        onBtnSmallClick();
    });
    $("#btn-medium").on("click", function () {
        onBtnMediumClick();
    });
    $("#btn-large").on("click", function () {
        onBtnLargeClick();
    });
    $("#btn-haisan").on("click", function () {
        onBtnHaiSanClick();
    });
    $("#btn-hawai").on("click", function () {
        onBtnHawaiClick();
    });
    $("#btn-bacon").on("click", function () {
        onBtnBaconClick();
    });

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnSmallClick() { // khi nút Small được ấn
    changeColorSize(gPLAN_SMALL);
    var vSelectedPlan = getPlan(gPLAN_SMALL, "20cm", 2, "200g", 2, "150.000");

    vSelectedPlan.displaySizeInConsoleLog()
}
function onBtnMediumClick() {  // khi nút Medium được ấn
    changeColorSize(gPLAN_MEDIUM);
    var vSelectedPlan = getPlan(gPLAN_MEDIUM, "25cm", 4, "300g", 3, "200.000");

    vSelectedPlan.displaySizeInConsoleLog();
}
function onBtnLargeClick() {   // khi nút Large được ấn
    changeColorSize(gPLAN_LARGE);
    var vSelectedPlan = getPlan(gPLAN_LARGE, "30cm", 8, "500g", 4, "250.000");

    vSelectedPlan.displaySizeInConsoleLog();
}
function onBtnHaiSanClick() { // khi nút Pizza Hải Sản được ấn
    changeColorPizza(gPLAN_HAI_SAN);
    displayPizzaInConsoleLog(gPLAN_HAI_SAN);
}
function onBtnHawaiClick() { // khi nút Pizza Hawai được ấn
    changeColorPizza(gPLAN_HAWAI);
    displayPizzaInConsoleLog(gPLAN_HAWAI);
}
function onBtnBaconClick() { // khi nút Pizza BAcon được ấn
    changeColorPizza(gPLAN_BACON);
    displayPizzaInConsoleLog(gPLAN_BACON);
}




/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm đổi màu nút chọn size pizza
function changeColorSize(paramColor) {
    var vBtnBasic = $("#btn-small");
    var vBtnMedium = $("#btn-medium");
    var vBtnLarge = $("#btn-large");

    if (paramColor === gPLAN_SMALL) {
        vBtnBasic
            .prop("class", "btn btn-info w-100")
            .attr("data-is-selected-menu", "Y");
        vBtnMedium
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-menu", "N");
        vBtnLarge
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-menu", "N");
    }

    else if (paramColor === gPLAN_MEDIUM) {
        vBtnBasic
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-menu", "N");
        vBtnMedium
            .prop("class", "btn btn-info w-100")
            .attr("data-is-selected-menu", "Y");
        vBtnLarge
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-menu", "N");
    }

    else if (paramColor === gPLAN_LARGE) {
        vBtnBasic
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-menu", "N");
        vBtnMedium
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-menu", "N");
        vBtnLarge
            .prop("class", "btn btn-info w-100")
            .attr("data-is-selected-menu", "Y");
    }
}
function getPlan(paramSize, pagramDuongKing, paramSuonNuong, paramSalad, paramDrink, paramPrice) {
    var vPlan = {
        size: paramSize,
        duongKinh: pagramDuongKing,
        suongNuong: paramSuonNuong,
        salad: paramSalad,
        drink: paramDrink,
        price: paramPrice,

        displaySizeInConsoleLog() {
            console.log("%cPLAN MENU COMBO - ...", "color:green");
            console.log(this.size);
            console.log("Đường kính: " + this.duongKinh);
            console.log("Sườn Nướng: " + this.suongNuong);
            console.log("Salad: " + this.salad);
            console.log("Drink: " + this.drink);
            console.log("PriceVND: " + this.price);
        }

    }
    return vPlan
}
// hàm chọn loại pizza
function changeColorPizza(paramColor) {
    var vBtnHaiSan = $("#btn-haisan");
    var vBtnHawai = $("#btn-hawai");
    var vBtnBacon = $("#btn-bacon");
    if (paramColor === gPLAN_HAI_SAN) {
        vBtnHaiSan
            .prop("class", "btn btn-info w-100")
            .attr("data-is-selected-pizza", "Y")
        vBtnHawai
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-pizza", "N")
        vBtnBacon
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-pizza", "N")
    }
    else if (paramColor === gPLAN_HAWAI) {
        vBtnHaiSan
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-pizza", "N")
        vBtnHawai
            .prop("class", "btn btn-info w-100")
            .attr("data-is-selected-pizza", "Y")
        vBtnBacon
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-pizza", "N")
    }
    else if (paramColor === gPLAN_BACON) {
        vBtnHaiSan
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-pizza", "N")
        vBtnHawai
            .prop("class", "btn btn-warning w-100")
            .attr("data-is-selected-pizza", "N")
        vBtnBacon
            .prop("class", "btn btn-info w-100")
            .attr("data-is-selected-pizza", "Y")
    }
}
function displayPizzaInConsoleLog(pagramPizza) {
    if (pagramPizza === gPLAN_HAI_SAN) {
        console.log("%c Pizza Hải sản", "color: blue");
    }
     if (pagramPizza === gPLAN_HAWAI) {
        console.log("%c Pizza Hawai", "color: red");
    }
    if (pagramPizza === gPLAN_BACON) {
        console.log("%c Pizza Bacon", "color: green");
    }
}

// load trang thêm option cho select drink
function onPageLoading() {
    "use strict";

    $.ajax({
      url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
      type: "GET",
      dataType: "json",
      success: function (pagramRes) {
      loadDataToSelectDrink(pagramRes);
      },
      error: function (ajaxContent) {
        alert(ajaxContent.responseText);
      },
    });
  }

  // hàm loadDataToSelect Drink
  function loadDataToSelectDrink(paramDrink){
    $.each(paramDrink,function (text, param) {
        $("#select-drink").append(
          $("<option>", {
            text: param.tenNuocUong,
            value: param.maNuocUong,
          })
        );
      });
  }
  
  
